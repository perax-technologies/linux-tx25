/*
 * Copyright 2004-2007 Freescale Semiconductor, Inc. All Rights Reserved.
 */

/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#ifndef __ASM_ARCH_MXC_MEMORY_H__
#define __ASM_ARCH_MXC_MEMORY_H__

#include <mach/hardware.h>

#if defined CONFIG_ARCH_MX1
#define PHYS_OFFSET		UL(0x08000000)
#elif defined CONFIG_ARCH_MX2
#ifdef CONFIG_ARCH_MX21
#define PHYS_OFFSET		UL(0xC0000000)
#endif
#ifdef CONFIG_ARCH_MX27
#define PHYS_OFFSET		UL(0xA0000000)
#endif
#elif defined CONFIG_ARCH_MX3
#define PHYS_OFFSET		UL(0x80000000)
#elif defined CONFIG_ARCH_MX25
#define PHYS_OFFSET		UL(0x80000000)
#elif defined CONFIG_ARCH_MX5
#define PHYS_OFFSET		UL(0x90000000)
#else
#error No PHYS_OFFSET defined for this architecture
#endif

#if defined(CONFIG_MX1_VIDEO)
/*
 * Increase size of DMA-consistent memory region.
 * This is required for i.MX camera driver to capture at least four VGA frames.
 */
#define CONSISTENT_DMA_SIZE SZ_4M
#endif /* CONFIG_MX1_VIDEO */

#if defined(CONFIG_MX3_VIDEO)
/*
 * Increase size of DMA-consistent memory region.
 * This is required for mx3 camera driver to capture at least two QXGA frames.
 */
#define CONSISTENT_DMA_SIZE SZ_8M
#endif /* CONFIG_MX3_VIDEO */

#ifdef CONFIG_ARCH_MX51
#if defined(CONSISTENT_DMA_SIZE) && CONSISTENT_DMA_SIZE < SZ_16M
#undef CONSISTENT_DMA_SIZE
#endif
#ifndef CONSISTENT_DMA_SIZE
#define CONSISTENT_DMA_SIZE SZ_16M
#endif /* CONSISTENT_DMA_SIZE */
#endif /* CONFIG_ARCH_MX51 */

#endif /* __ASM_ARCH_MXC_MEMORY_H__ */
