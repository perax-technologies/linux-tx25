/*
 * Copyright (C) 2009  Lothar Wassmann <LW@KARO-electronics.de>
 *
 * derived from: arch/arm/plat-mxc/include/mach/mx25.h
 *    Copyright 2008-2009 Freescale Semiconductor, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the:
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301
 */

#ifndef __MACH_MX25_H__
#define __MACH_MX25_H__

#define SDMA_V2

#define CSD0_BASE_ADDR			0x80000000

#define MX25_AIPS1_BASE_ADDR		UL(0x43F00000)
#define MX25_AIPS1_BASE_ADDR_VIRT	0xFC000000
#define MX25_AIPS1_SIZE			SZ_1M
#define MX25_AIPS2_BASE_ADDR		UL(0x53F00000)
#define MX25_AIPS2_BASE_ADDR_VIRT	0xFC200000
#define MX25_AIPS2_SIZE			SZ_1M
#define MX25_AVIC_BASE_ADDR		UL(0x68000000)
#define MX25_AVIC_BASE_ADDR_VIRT	0xFC400000
#define MX25_AVIC_SIZE			SZ_1M
#define MX25_SPBA0_BASE_ADDR		UL(0x50000000)
#define MX25_SPBA0_BASE_ADDR_VIRT	0xFC100000
#define MX25_SPBA0_SIZE			SZ_1M

#define MX25_IOMUXC_BASE_ADDR		(MX25_AIPS1_BASE_ADDR + 0xac000)

#define MX25_CCM_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x80000)
#define MX25_GPT1_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x90000)
#define MX25_WDOG_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0xdc000)

#define MX25_GPIO1_BASE_ADDR_VIRT	(MX25_AIPS2_BASE_ADDR_VIRT + 0xcc000)
#define MX25_GPIO2_BASE_ADDR_VIRT	(MX25_AIPS2_BASE_ADDR_VIRT + 0xd0000)
#define MX25_GPIO3_BASE_ADDR_VIRT	(MX25_AIPS2_BASE_ADDR_VIRT + 0xa4000)
#define MX25_GPIO4_BASE_ADDR_VIRT	(MX25_AIPS2_BASE_ADDR_VIRT + 0x9c000)

#define MX25_AIPS1_IO_ADDRESS(x)  \
	(((x) - MX25_AIPS1_BASE_ADDR) + MX25_AIPS1_BASE_ADDR_VIRT)
#define MX25_AIPS2_IO_ADDRESS(x)  \
	(((x) - MX25_AIPS2_BASE_ADDR) + MX25_AIPS2_BASE_ADDR_VIRT)
#define MX25_AVIC_IO_ADDRESS(x)  \
	(((x) - MX25_AVIC_BASE_ADDR) + MX25_AVIC_BASE_ADDR_VIRT)

#define __in_range(addr, name)	((addr) >= name##_BASE_ADDR &&		\
					(addr) < name##_BASE_ADDR + name##_SIZE)

#define MX25_IO_ADDRESS(x)					\
	(void __force __iomem *)				\
	(__in_range(x, MX25_AIPS1) ? MX25_AIPS1_IO_ADDRESS(x) :	\
	__in_range(x, MX25_AIPS2) ? MX25_AIPS2_IO_ADDRESS(x) :	\
	__in_range(x, MX25_AVIC) ? MX25_AVIC_IO_ADDRESS(x) :	\
	0)

#define MAX_BASE_ADDR		(MX25_AIPS1_BASE_ADDR + 0x00004000)
#define CLKCTL_BASE_ADDR	(MX25_AIPS1_BASE_ADDR + 0x00008000)
#define ETB_SLOT4_BASE_ADDR	(MX25_AIPS1_BASE_ADDR + 0x0000C000)
#define ETB_SLOT5_BASE_ADDR	(MX25_AIPS1_BASE_ADDR + 0x00010000)
#define AAPE_BASE_ADDR		(MX25_AIPS1_BASE_ADDR + 0x00014000)
#define I2C_BASE_ADDR		(MX25_AIPS1_BASE_ADDR + 0x00080000)
#define I2C3_BASE_ADDR		(MX25_AIPS1_BASE_ADDR + 0x00084000)
#define CAN1_BASE_ADDR		(MX25_AIPS1_BASE_ADDR + 0x00088000)
#define CAN2_BASE_ADDR		(MX25_AIPS1_BASE_ADDR + 0x0008C000)
#define UART1_BASE_ADDR		(MX25_AIPS1_BASE_ADDR + 0x00090000)
#define UART2_BASE_ADDR		(MX25_AIPS1_BASE_ADDR + 0x00094000)
#define I2C2_BASE_ADDR		(MX25_AIPS1_BASE_ADDR + 0x00098000)
#define OWIRE_BASE_ADDR		(MX25_AIPS1_BASE_ADDR + 0x0009C000)
#define ATA_BASE_ADDR		(MX25_AIPS1_BASE_ADDR + 0x000A0000)
#define CSPI1_BASE_ADDR		(MX25_AIPS1_BASE_ADDR + 0x000A4000)
#define KPP_BASE_ADDR		(MX25_AIPS1_BASE_ADDR + 0x000A8000)
#define IOMUXC_BASE_ADDR	(MX25_AIPS1_BASE_ADDR + 0x000AC000)
#define AUDMUX_BASE_ADDR	(MX25_AIPS1_BASE_ADDR + 0x000B0000)
#define ECT_A_BASE_ADDR		(MX25_AIPS1_BASE_ADDR + 0x000B8000)
#define ECT_B_BASE_ADDR		(MX25_AIPS1_BASE_ADDR + 0x000BC000)

#define CSPI3_BASE_ADDR		(MX25_SPBA0_BASE_ADDR + 0x00004000)
#define UART4_BASE_ADDR		(MX25_SPBA0_BASE_ADDR + 0x00008000)
#define UART3_BASE_ADDR		(MX25_SPBA0_BASE_ADDR + 0x0000C000)
#define CSPI2_BASE_ADDR		(MX25_SPBA0_BASE_ADDR + 0x00010000)
#define SSI2_BASE_ADDR		(MX25_SPBA0_BASE_ADDR + 0x00014000)
#define ESAI_BASE_ADDR		(MX25_SPBA0_BASE_ADDR + 0x00018000)
#define ATA_DMA_BASE_ADDR	(MX25_SPBA0_BASE_ADDR + 0x00020000)
#define SIM1_BASE_ADDR		(MX25_SPBA0_BASE_ADDR + 0x00024000)
#define SIM2_BASE_ADDR		(MX25_SPBA0_BASE_ADDR + 0x00028000)
#define UART5_BASE_ADDR		(MX25_SPBA0_BASE_ADDR + 0x0002C000)
#define TSC_BASE_ADDR		(MX25_SPBA0_BASE_ADDR + 0x00030000)
#define SSI1_BASE_ADDR		(MX25_SPBA0_BASE_ADDR + 0x00034000)
#define FEC_BASE_ADDR		(MX25_SPBA0_BASE_ADDR + 0x00038000)
#define SPBA_CTRL_BASE_ADDR	(MX25_SPBA0_BASE_ADDR + 0x0003C000)

#define CCM_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x00080000)
#define GPT4_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x00084000)
#define GPT3_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x00088000)
#define GPT2_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x0008C000)
#define GPT1_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x00090000)
#define EPIT1_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x00094000)
#define EPIT2_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x00098000)
#define GPIO4_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x0009C000)
#define PWM2_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x000A0000)
#define GPIO3_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x000A4000)
#define PWM3_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x000A8000)
#define SCC_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x000AC000)
#define RNGD_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x000B0000)
#define MMC_SDHC1_BASE_ADDR	(MX25_AIPS2_BASE_ADDR + 0x000B4000)
#define MMC_SDHC2_BASE_ADDR	(MX25_AIPS2_BASE_ADDR + 0x000B8000)
#define LCDC_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x000BC000)
#define SLCDC_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x000C0000)
#define PWM4_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x000C8000)
#define GPIO1_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x000CC000)
#define GPIO2_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x000D0000)
#define SDMA_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x000D4000)
#define WDOG_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x000DC000)
#define PWM1_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x000E0000)
#define RTIC_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x000EC000)
#define IIM_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x000F0000)
#define USBOTG_BASE_ADDR	(MX25_AIPS2_BASE_ADDR + 0x000F4000)
#define OTG_BASE_ADDR		(USBOTG_BASE_ADDR + 0x000)
#define USBH2_BASE_ADDR		(USBOTG_BASE_ADDR + 0x400)
#define CSI_BASE_ADDR		(MX25_AIPS2_BASE_ADDR + 0x000F8000)
#define MX25_DRYICE_BASE_ADDR	(MX25_AIPS2_BASE_ADDR + 0x000FC000)

#define MX25_CCM_BASE		MX25_IO_ADDRESS(MX25_CCM_BASE_ADDR)

#define CCM_MPCTL		0x00
#define CCM_UPCTL		0x04
#define CCM_CCTL		0x08
#define CCM_CGCR0		0x0C
#define CCM_CGCR1		0x10
#define CCM_CGCR2		0x14
#define CCM_PCDR0		0x18
#define CCM_PCDR1		0x1C
#define CCM_PCDR2		0x20
#define CCM_PCDR3		0x24
#define CCM_RCSR		0x28
#define CCM_CRDR		0x2C
#define CCM_DCVR0		0x30
#define CCM_DCVR1		0x34
#define CCM_DCVR2		0x38
#define CCM_DCVR3		0x3c
#define CCM_LTR0		0x40
#define CCM_LTR1		0x44
#define CCM_LTR2		0x48
#define CCM_LTR3		0x4c
#define CCM_MCR			0x64

#define X_MEMC_BASE_ADDR	UL(0xB8000000)
#define SDRAMC_BASE_ADDR	(X_MEMC_BASE_ADDR + 0x1000)
#define WEIM_BASE_ADDR		(X_MEMC_BASE_ADDR + 0x2000)
#define M3IF_BASE_ADDR		(X_MEMC_BASE_ADDR + 0x3000)
#define EMI_CTL_BASE_ADDR	(X_MEMC_BASE_ADDR + 0x4000)

/*!
 * Defines for modules using static and dynamic DMA channels
 */
#define MXC_DMA_CHANNEL_IRAM		30
#define MXC_DMA_CHANNEL_UART1_RX	MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_UART1_TX	MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_UART2_RX	MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_UART2_TX	MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_UART3_RX	MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_UART3_TX	MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_UART4_RX	MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_UART4_TX	MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_UART5_RX	MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_UART5_TX	MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_MMC1		MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_SSI1_RX		MXC_DMA_DYNAMIC_CHANNEL
#ifdef CONFIG_SDMA_IRAM
#define MXC_DMA_CHANNEL_SSI1_TX		(MXC_DMA_CHANNEL_IRAM + 1)
#else
#define MXC_DMA_CHANNEL_SSI1_TX		MXC_DMA_DYNAMIC_CHANNEL
#endif
#define MXC_DMA_CHANNEL_SSI2_RX		MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_SSI2_TX		MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_CSPI1_RX	MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_CSPI1_TX	MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_CSPI2_RX	MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_CSPI2_TX	MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_CSPI3_RX	MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_CSPI3_TX	MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_ATA_RX		MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_ATA_TX		MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_MEMORY		MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_ESAI_RX		MXC_DMA_DYNAMIC_CHANNEL
#define MXC_DMA_CHANNEL_ESAI_TX		MXC_DMA_DYNAMIC_CHANNEL

/*
 * DMA request assignments
 */
#define DMA_REQ_EXTREQ0	   0
#define DMA_REQ_CCM	   1
#define DMA_REQ_ATA_TX_END 2
#define DMA_REQ_ATA_TX	   3
#define DMA_REQ_ATA_RX	   4
#define DMA_REQ_CSPI2_RX   6
#define DMA_REQ_CSPI2_TX   7
#define DMA_REQ_CSPI1_RX   8
#define DMA_REQ_CSPI1_TX   9
#define DMA_REQ_UART3_RX   10
#define DMA_REQ_UART3_TX   11
#define DMA_REQ_UART4_RX   12
#define DMA_REQ_UART4_TX   13
#define DMA_REQ_EXTREQ1	   14
#define DMA_REQ_EXTREQ2	   15
#define DMA_REQ_UART2_RX   16
#define DMA_REQ_UART2_TX   17
#define DMA_REQ_UART1_RX   18
#define DMA_REQ_UART1_TX   19
#define DMA_REQ_SSI2_RX1   22
#define DMA_REQ_SSI2_TX1   23
#define DMA_REQ_SSI2_RX0   24
#define DMA_REQ_SSI2_TX0   25
#define DMA_REQ_SSI1_RX1   26
#define DMA_REQ_SSI1_TX1   27
#define DMA_REQ_SSI1_RX0   28
#define DMA_REQ_SSI1_TX0   29
#define DMA_REQ_NFC	   30
#define DMA_REQ_ECT	   31
#define DMA_REQ_ESAI_RX	   32
#define DMA_REQ_ESAI_TX	   33
#define DMA_REQ_CSPI3_RX   34
#define DMA_REQ_CSPI3_TX   35
#define DMA_REQ_SIM2_RX	   36
#define DMA_REQ_SIM2_TX	   37
#define DMA_REQ_SIM1_RX	   38
#define DMA_REQ_SIM1_TX	   39
#define DMA_REQ_TSC_GCQ	   44
#define DMA_REQ_TSC_TCQ	   45
#define DMA_REQ_UART5_RX   46
#define DMA_REQ_UART5_TX   47

/*
 *  Interrupt numbers
 */
#define MXC_INT_CSPI3		 0
#define MXC_INT_GPT4		 1
#define MXC_INT_OWIRE		 2
#define MXC_INT_I2C		 3
#define MXC_INT_I2C2		 4
#define MXC_INT_UART4		 5
#define MXC_INT_RTIC		 6
#define MXC_INT_ESAI		 7
#define MXC_INT_SDHC2		 8
#define MXC_INT_SDHC1		 9
#define MXC_INT_I2C3		10
#define MXC_INT_SSI2		11
#define MXC_INT_SSI1		12
#define MXC_INT_CSPI2		13
#define MXC_INT_CSPI1		14
#define MXC_INT_ATA		15
#define MXC_INT_GPIO3		16
#define MXC_INT_CSI		17
#define MXC_INT_UART3		18
#define MXC_INT_IIM		19
#define MXC_INT_SIM1		20
#define MXC_INT_SIM2		21
#define MXC_INT_RNGD		22
#define MXC_INT_GPIO4		23
#define MXC_INT_KPP		24
#define MXC_INT_DRYICE_RTC	25
#define MXC_INT_PWM		26
#define MXC_INT_EPIT2		27
#define MXC_INT_EPIT1		28
#define MXC_INT_GPT3		29
#define MXC_INT_POWER_FAIL	30
#define MXC_INT_CCM		31
#define MXC_INT_UART2		32
#define MXC_INT_NANDFC		33
#define MXC_INT_SDMA		34
#define MXC_INT_USB_H2		35
#define MXC_INT_PWM2		36
#define MXC_INT_USB_OTG		37
#define MXC_INT_SLCDC		38
#define MXC_INT_LCDC		39
#define MXC_INT_UART5		40
#define MXC_INT_PWM3		41
#define MXC_INT_PWM4		42
#define MXC_INT_CAN1		43
#define MXC_INT_CAN2		44
#define MXC_INT_UART1		45
#define MXC_INT_TSC		46
#define MXC_INT_ECT		48
#define MXC_INT_SCC_SCM		49
#define MXC_INT_SCC_SMN		50
#define MXC_INT_GPIO2		51
#define MXC_INT_GPIO1		52
#define MXC_INT_GPT2		53
#define MXC_INT_GPT1		54
#define MXC_INT_WDOG		55
#define MXC_INT_DRYICE		56
#define MXC_INT_FEC		57
#define MXC_INT_EXT_INT5	58
#define MXC_INT_EXT_INT4	59
#define MXC_INT_EXT_INT3	60
#define MXC_INT_EXT_INT2	61
#define MXC_INT_EXT_INT1	62
#define MXC_INT_EXT_INT0	63

#define MXC_INT_GPT		MXC_INT_GPT1


#endif /* __MACH_MX25_H__ */
