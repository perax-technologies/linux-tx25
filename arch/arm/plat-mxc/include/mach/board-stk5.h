/*
 * Copyright 2009 <LW@KARO-electronics.de>
 *
 * The code contained herein is licensed under the GNU General Public
 * License. You may obtain a copy of the GNU General Public License
 * Version 2 or later at the following locations:
 *
 * http://www.opensource.org/licenses/gpl-license.html
 * http://www.gnu.org/copyleft/gpl.html
 */

#define UART1_ENABLED		1
#define UART2_ENABLED		1
#define UART3_ENABLED		0
/* Not available on TX25 */
#define UART4_ENABLED		1
#define UART5_ENABLED		0
