/*
 * Copyright (C) 2009  Lothar Wassmann <LW@KARO-electronics.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the:
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301
 */

extern struct platform_device mxc_uart_device0;
extern struct platform_device mxc_uart_device1;
extern struct platform_device mxc_uart_device2;
extern struct platform_device mxc_uart_device3;
extern struct platform_device mxc_uart_device4;
extern struct platform_device mxc_otg_device;
extern struct platform_device mxc_otg_udc_device;
extern struct platform_device mxc_usbh2_device;
extern struct platform_device mxc_spi_device0;
extern struct platform_device mxc_spi_device1;
extern struct platform_device mxc_spi_device2;
extern struct platform_device mxc_pwm_device0;
extern struct platform_device mxc_pwm_device1;
extern struct platform_device mxc_pwm_device2;
extern struct platform_device mxc_pwm_device3;
extern struct platform_device imx_keypad_device;
extern struct platform_device mx25_i2c1_device;
extern struct platform_device mx25_i2c2_device;
extern struct platform_device mx25_i2c3_device;
extern struct platform_device mx25_fb_device;
extern struct platform_device mx25_rtc_device;
extern struct platform_device mx25_tsc_device;
extern struct platform_device mx25_wdt_device;
extern struct platform_device mxc_audmux_v3_device;
