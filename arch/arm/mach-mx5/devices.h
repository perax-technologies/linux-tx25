/*
 * Copyright (C) 2009  Lothar Wassmann <LW@KARO-electronics.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the:
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301
 */

extern struct platform_device mx51_uart_device1;
extern struct platform_device mx51_uart_device2;
extern struct platform_device mx51_uart_device3;
extern struct platform_device mx51_w1_master_device;
extern struct platform_device mx51_nand_device;
extern struct platform_device mx51_i2c1_device;
extern struct platform_device mx51_i2c2_device;
extern struct platform_device mx51_i2c_hs_device;
extern struct platform_device mx51_spi1_device;
extern struct platform_device mx51_spi2_device;
extern struct platform_device mx51_spi3_device;
extern struct platform_device mx51_ipu_device;
extern struct platform_device mx51_fb_device;
extern struct platform_device mx51_camera;
extern struct platform_device mx51_fec_device;
extern struct platform_device mx51_esdhc1_device;
extern struct platform_device mx51_esdhc2_device;
extern struct platform_device mx51_usbotg_device;
extern struct platform_device mx51_usbh1_device;
extern struct platform_device mx51_usbh2_device;
extern struct platform_device mx51_otg_udc_device;
extern struct platform_device mx51_rnga_device;
extern struct platform_device mx51_wdt_device;
extern struct platform_device mx51_srtc_device;
extern struct platform_device mxc_audmux_v3_device;
