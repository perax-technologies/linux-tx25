/*
 * Copyright (C) 2009  Lothar Wassmann <LW@KARO-electronics.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the:
 * Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301
 */

#define SSI_SACCEN	0x54
#define SSI_SACCDIS	0x58

/*
 * SCR Register bit shift definitions
 */
#define SSI_ENABLE_SHIFT	    0
#define SSI_TRANSMIT_ENABLE_SHIFT   1
#define SSI_RECEIVE_ENABLE_SHIFT    2
#define SSI_NETWORK_MODE_SHIFT	    3
#define SSI_SYNCHRONOUS_MODE_SHIFT  4
#define SSI_I2S_MODE_SHIFT	    5
#define SSI_SYSTEM_CLOCK_SHIFT	    7
#define SSI_TWO_CHANNEL_SHIFT	    8
#define SSI_CLOCK_IDLE_SHIFT	    9

/*
 * STCR & SRCR Registers bit shift definitions
 */
#define SSI_EARLY_FRAME_SYNC_SHIFT    0
#define SSI_FRAME_SYNC_LENGTH_SHIFT   1
#define SSI_FRAME_SYNC_INVERT_SHIFT   2
#define SSI_CLOCK_POLARITY_SHIFT      3
#define SSI_SHIFT_DIRECTION_SHIFT     4
#define SSI_CLOCK_DIRECTION_SHIFT     5
#define SSI_FRAME_DIRECTION_SHIFT     6
#define SSI_FIFO_ENABLE_0_SHIFT	      7
#define SSI_FIFO_ENABLE_1_SHIFT	      8
#define SSI_BIT_0_SHIFT		      9

/*
 * STCCR & SRCCR Registers bit shift definitions
 */
#define SSI_PRESCALER_MODULUS_SHIFT	     0
#define SSI_FRAME_RATE_DIVIDER_SHIFT	     8
#define SSI_WORD_LENGTH_SHIFT		    13
#define SSI_PRESCALER_RANGE_SHIFT	    17
#define SSI_DIVIDE_BY_TWO_SHIFT		    18
#define SSI_FRAME_DIVIDER_MASK		    31
#define SSI_MIN_FRAME_DIVIDER_RATIO	     1
#define SSI_MAX_FRAME_DIVIDER_RATIO	    32
#define SSI_PRESCALER_MODULUS_MASK	   255
#define SSI_MIN_PRESCALER_MODULUS_RATIO	     1
#define SSI_MAX_PRESCALER_MODULUS_RATIO	   256
#define SSI_WORD_LENGTH_MASK		    15
/*
 * SISR Register definition
 */
#define SSI_IRQ_STATUS_NUMBER	     19
/*
 * SFCSR Register bit shift definitions
 */
#define SSI_RX_FIFO_1_COUNT_SHIFT	28
#define SSI_TX_FIFO_1_COUNT_SHIFT	24
#define SSI_RX_FIFO_1_WATERMARK_SHIFT	20
#define SSI_TX_FIFO_1_WATERMARK_SHIFT	16
#define SSI_RX_FIFO_0_COUNT_SHIFT	12
#define SSI_TX_FIFO_0_COUNT_SHIFT	 8
#define SSI_RX_FIFO_0_WATERMARK_SHIFT	 4
#define SSI_TX_FIFO_0_WATERMARK_SHIFT	 0
#define SSI_MIN_FIFO_WATERMARK		 0
#define SSI_MAX_FIFO_WATERMARK		 8

/*
 * SSI Option Register (SOR) bit shift definitions
 */
#define SSI_FRAME_SYN_RESET_SHIFT	 0
#define SSI_WAIT_SHIFT			 1
#define SSI_INIT_SHIFT			 3
#define SSI_TRANSMITTER_CLEAR_SHIFT	 4
#define SSI_RECEIVER_CLEAR_SHIFT	 5
#define SSI_CLOCK_OFF_SHIFT		 6
#define SSI_WAIT_STATE_MASK	       0x3

/*
 * SSI AC97 Control Register (SACNT) bit shift definitions
 */
#define AC97_MODE_ENABLE_SHIFT		 0
#define AC97_VARIABLE_OPERATION_SHIFT	 1
#define AC97_TAG_IN_FIFO_SHIFT		 2
#define AC97_READ_COMMAND_SHIFT		 3
#define AC97_WRITE_COMMAND_SHIFT	 4
#define AC97_FRAME_RATE_DIVIDER_SHIFT	 5
#define AC97_FRAME_RATE_MASK	      0x3F

/*
 * SSI Test Register (STR) bit shift definitions
 */
#define SSI_TEST_MODE_SHIFT		  15
#define SSI_RCK2TCK_SHIFT		  14
#define SSI_RFS2TFS_SHIFT		  13
#define SSI_RXSTATE_SHIFT		  8
#define SSI_TXD2RXD_SHIFT		  7
#define SSI_TCK2RCK_SHIFT		  6
#define SSI_TFS2RFS_SHIFT		  5
#define SSI_TXSTATE_SHIFT		  0

#define SACC_SLOT(n)	(1 << (9 - ((n) - 3)))
#define SATAG_SLOT(n)	(1 << (15 - (n)))

#define STCCR_WL_MASK	(SSI_WORD_LENGTH_MASK << SSI_WORD_LENGTH_SHIFT)
#define STCCR_DC_MASK	(SSI_FRAME_DIVIDER_MASK << SSI_FRAME_RATE_DIVIDER_SHIFT)
#define STCCR_DIV2	(1 << SSI_DIVIDE_BY_TWO_SHIFT)

#define SRCCR_WL_MASK	(SSI_WORD_LENGTH_MASK << SSI_WORD_LENGTH_SHIFT)
#define SRCCR_DC_MASK	(SSI_FRAME_DIVIDER_MASK << SSI_FRAME_RATE_DIVIDER_SHIFT)
#define SRCCR_DIV2	(1 << SSI_DIVIDE_BY_TWO_SHIFT)

#define SCR_CLK_IST	(1 << 9)
#define SCR_TE		(1 << SSI_TRANSMIT_ENABLE_SHIFT)
#define SCR_RE		(1 << SSI_RECEIVE_ENABLE_SHIFT)
#define SCR_SYN		(1 << 4)
#define SCR_SSIEN	(1 << SSI_ENABLE_SHIFT)

#define SOR_CLKOFF	(1 << 6)
#define SOR_RX_CLR	(1 << 5)
#define SOR_TX_CLR	(1 << 4)
#define SOR_INIT	(1 << 3)

#define SISR_RFRC	(1 << 24) /* receive frame complete flag (on some CPUs) */
#define SISR_TFRC	(1 << 23) /* transmit frame complete flag (on some CPUs) */
#define SISR_CMDAU	(1 << 18)
#define SISR_CMDDU	(1 << 17)
#define SISR_RXT	(1 << 16)
#define SISR_RDR1	(1 << 15)
#define SISR_RDR0	(1 << 14)
#define SISR_TDE1	(1 << 13)
#define SISR_TDE0	(1 << 12)
#define SISR_ROE1	(1 << 11)
#define SISR_ROE0	(1 << 10)
#define SISR_TUE1	(1 << 9)
#define SISR_TUE0	(1 << 8)
#define SISR_TFS	(1 << 7)
#define SISR_RFS	(1 << 6)
#define SISR_TLS	(1 << 5)
#define SISR_RLS	(1 << 4)
#define SISR_RFF1	(1 << 3)
#define SISR_RFF0	(1 << 2)
#define SISR_TFE1	(1 << 1)
#define SISR_TFE0	(1 << 0)

#define SIER_RFRC_EN	(1 << 24) /* receive frame complete enable (on some CPUs) */
#define SIER_TFRC_EN	(1 << 23) /* transmit frame complete enable (on some CPUs) */
#define SIER_RDMAE	(1 << 22)
#define SIER_RIE	(1 << 21)
#define SIER_TDMAE	(1 << 20)
#define SIER_TIE	(1 << 19)
#define SIER_CMDAU_EN	(1 << 18)
#define SIER_CMDDU_EN	(1 << 17)
#define SIER_RXT_EN	(1 << 16)
#define SIER_RDR1_EN	(1 << 15)
#define SIER_RDR0_EN	(1 << 14)
#define SIER_TDE1_EN	(1 << 13)
#define SIER_TDE0_EN	(1 << 12)
#define SIER_ROE1_EN	(1 << 11)
#define SIER_ROE0_EN	(1 << 10)
#define SIER_TUE1_EN	(1 << 9)
#define SIER_TUE0_EN	(1 << 8)
#define SIER_TFS_EN	(1 << 7)
#define SIER_RFS_EN	(1 << 6)
#define SIER_TLS_EN	(1 << 5)
#define SIER_RLS_EN	(1 << 4)
#define SIER_RFF1_EN	(1 << 3)
#define SIER_RFF0_EN	(1 << 2)
#define SIER_TFE1_EN	(1 << 1)
#define SIER_TFE0_EN	(1 << 0)

#define SACNT_FRDIV_SHIFT	5
#define SACNT_FRDIV_BITS	6
#define SACNT_FRDIV_MASK	(((1 << SACNT_FRDIV_BITS) - 1) << SACNT_FRDIV_SHIFT)
#define SACNT_WR	(1 << AC97_WRITE_COMMAND_SHIFT)
#define SACNT_RD	(1 << AC97_READ_COMMAND_SHIFT)
#define SACNT_FV	(1 << 1)
#define SACNT_AC97EN	(1 << 0)

#define STCR_TXBIT0	(1 << 9)
#define STCR_TFEN1	(1 << 8)
#define STCR_TFEN0	(1 << 7)

#define SRCR_RXEXT	(1 << 10)
#define SRCR_RXBIT0	(1 << 9)
#define SRCR_RFEN1	(1 << 8)
#define SRCR_RFEN0	(1 << 7)
